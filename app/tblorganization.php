<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class tblorganization extends Model
{
	protected $table = 'tblorganization';
	public $timestamps = false;
}
