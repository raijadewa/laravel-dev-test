<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class tbldrivers extends Model
{
	protected $table = 'tbldrivers';
	public $timestamps = false;
}
