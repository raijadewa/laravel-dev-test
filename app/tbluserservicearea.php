<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class tbluserservicearea extends Model
{
	protected $table = 'tbluserservicearea';
	public $timestamps = false;
}
