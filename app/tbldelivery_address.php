<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class tbldelivery_address extends Model
{
	protected $table = 'tbldelivery_address';
	public $timestamps = false;
}
