<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class tbltasks extends Model
{
	protected $table = 'tbltasks';
	public $timestamps = false;
}
