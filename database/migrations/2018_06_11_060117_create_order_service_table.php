<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrderServiceTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tblorderservice', function (Blueprint $table) {
            $table->increments('idOrderService');
            $table->integer('idBinService');
			$table->integer('idConsumer');
			$table->string('paymentUniqueCode',20);
			$table->date('serviceStart');
			$table->date('serviceEnd');
			$table->float('totalServiceCharge');
			$table->date('collectionDate');
			$table->text('deliveryAddress');
			$table->text('deliveryComments');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('order_service');
    }
}
